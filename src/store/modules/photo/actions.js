import { SET_PHOTOS } from './mutationTypes';
import api from '../../../api/Api';
import {SET_LOADING} from "../../mutationTypes";
import { photoMapper } from  '../../../service/Normalizer';
export default {
    async fetchPhotos({ commit }) {
        commit(SET_LOADING, true, { root: true });

        try {
            const photos = await api.get('/photos');

            let photosData = photos.data;

            commit(SET_PHOTOS, photosData);
            commit(SET_LOADING, false, { root: true });

            return Promise.resolve(
                photosData.map(photoMapper)
            );
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },

    async fetchPhotosByAlbumId({ commit }) {
        commit(SET_LOADING, true, { root: true });

        try {
            const photos = await api.get('/photos?albumId=1');

            let photosData = photos.data;

            commit(SET_PHOTOS, photosData);
            commit(SET_LOADING, false, { root: true });

            return Promise.resolve(
                photosData.map(photoMapper)
            );
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },
};
