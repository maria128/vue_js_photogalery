import { SET_PHOTOS } from './mutationTypes';
import { photoMapper } from '../../../service/Normalizer';

export default {
    [SET_PHOTOS]: (state, photos) => {
        state.photos = {
            ...state.photos,
            ...photos.reduce(
                (prev, photo) => ({ ...prev, [photo.id]: photoMapper(photo) }),
                {}
            ),
        };
    },
};
