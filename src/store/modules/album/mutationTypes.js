export const SET_ALBUMS = 'setAlbums';
export const SET_ALBUM = 'editAlbum';
export const DELETE_ALBUM = 'deleteAlbum';
