export default {
    albumSortedByCreatedDate: state => state.albums,
    getAlbumById: state => id => state.albums[id],
};
