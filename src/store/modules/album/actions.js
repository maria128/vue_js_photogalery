import { SET_ALBUMS, SET_ALBUM, DELETE_ALBUM } from './mutationTypes';
import api from '../../../api/Api';
import {SET_LOADING} from "../../mutationTypes";
import { albumMapper } from  '../../../service/Normalizer';
export default {
    async fetchAlbums({ commit }) {
        commit(SET_LOADING, true, { root: true });

        try {
            const albums = await api.get('/albums');

            let albumsData = albums.data;

            commit(SET_ALBUMS, albumsData);
            commit(SET_LOADING, false, { root: true });

            return Promise.resolve(
                albumsData.map(albumMapper)
            );
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },
    async fetchAlbumsById({ commit },  id ) {

        commit(SET_LOADING, true, { root: true });

        try {
            const album = await api.get(`/albums/${id}`);

            commit(SET_LOADING, false, { root: true });
            commit(SET_ALBUM, album.data);

            return Promise.resolve(albumMapper(album.data));
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },

    async editAlbum({ commit }, { id, title, url, userId }) {
        commit(SET_LOADING, true, { root: true });

        try {
            const formData = new FormData();
            formData.append('url', url);
            formData.append('title', title);
            formData.append('userId', userId);

            const album = await api.put(`/albums/${id}`, formData);

            commit(SET_ALBUM, album);
            commit(SET_LOADING, false, { root: true });

            return Promise.resolve(albumMapper(album));
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },
    async deleteAlbum({ commit },  id ) {
        commit(SET_LOADING, true, { root: true });

        try {
            await api.delete(`/albums/${id}`);
            commit(DELETE_ALBUM, id);
            commit(SET_LOADING, false, { root: true });
            return Promise.resolve();
        } catch (error) {
            commit(SET_LOADING, false, { root: true });
            return Promise.reject(error);
        }
    },


    async addAlbum({ commit }, { title, url, userId }) {

        commit(SET_LOADING, true, { root: true });

        try {
            const formData = new FormData();
            formData.append('url', url);
            formData.append('title', title);
            formData.append('userId', userId);

            const album = await api.post(`/albums`, formData);
            commit(SET_ALBUM, album);
            commit(SET_LOADING, false, { root: true });
            return Promise.resolve(albumMapper(album));
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },
};
