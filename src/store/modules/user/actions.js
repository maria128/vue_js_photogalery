import { SET_USERS, SET_USER, DELETE_USER } from './mutationTypes';
import api from '../../../api/Api';
import {SET_LOADING} from "../../mutationTypes";
import { userMapper } from  '../../../service/Normalizer';
export default {
    async fetchUsers({ commit }) {
        commit(SET_LOADING, true, { root: true });

        try {
            const users = await api.get('/users');

            let usersData = users.data;

            commit(SET_USERS, usersData);
            commit(SET_LOADING, false, { root: true });

            return Promise.resolve(
                usersData.map(userMapper)
            );
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },
    async fetchUserByUserId({ commit },  userId ) {

        commit(SET_LOADING, true, { root: true });

        try {
            const user = await api.get(`/users/${userId}`);

            commit(SET_LOADING, false, { root: true });
            commit(SET_USER, user.data);

            return Promise.resolve(userMapper(user.data));
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },

    async editUser({ commit }, { id, name, email, avatar }) {
        commit(SET_LOADING, true, { root: true });

        try {
            const formData = new FormData();
            formData.append('avatar', avatar);
            formData.append('name', name);
            formData.append('email', email);

            const user = await api.put(`/users/${id}`, formData);

            commit(SET_USER, user);
            commit(SET_LOADING, false, { root: true });

            return Promise.resolve(userMapper(user));
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },
    async deleteUser({ commit },  id ) {
        commit(SET_LOADING, true, { root: true });

        try {

            await api.delete(`/users/${id}`);

            commit(DELETE_USER, id);
            commit(SET_LOADING, false, { root: true });

            return Promise.resolve();
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },


    async addUser({ commit }, { name, email, avatar }) {

        commit(SET_LOADING, true, { root: true });

        try {
            const formData = new FormData();
            formData.append('avatar', avatar);
            formData.append('name', name);
            formData.append('email', email);

            const user = await api.post(`/users`, formData);

            commit(SET_USER, user);
            commit(SET_LOADING, false, { root: true });

            return Promise.resolve(userMapper(user));
        } catch (error) {
            commit(SET_LOADING, false, { root: true });

            return Promise.reject(error);
        }
    },
};
