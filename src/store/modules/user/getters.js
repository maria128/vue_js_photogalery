export default {
    usersSortedByCreatedDate: state => state.users,
    getUserById: state => id => state.users[id],
};
