import { SET_USERS, SET_USER, DELETE_USER } from './mutationTypes';
import { userMapper } from '../../../service/Normalizer';

export default {
    [SET_USERS]: (state, users) => {
        state.users = {
            ...state.users,
            ...users.reduce(
                (prev, user) => ({ ...prev, [user.id]: userMapper(user) }),
                {}
            ),
        };
    },

    [SET_USER]: (state, user) => {
        state.users = {
            ...state.users,
            [user.id]: userMapper(user)
        };
    },

    [DELETE_USER]: (state, id) => {
        delete state.users[id];
    },

};
