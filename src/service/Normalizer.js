
export const userMapper = user => ({
    id: user.id,
    avatar: user.avatar,
    email: user.email,
    name: user.name,
});

export const emptyUser = () => ({
    id: null,
    avatar: null,
    email: '',
    name: '',
});


export const albumMapper = album => ({
    id: album.id,
    title: album.title,
    url: album.url,
    userId: album.userId,
});

export const photoMapper = photo => ({
    id: photo.id,
    title: photo.title,
    url: photo.url,
    albumId: photo.albumId
});
