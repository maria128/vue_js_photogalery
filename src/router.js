import Vue from 'vue';
import Router from 'vue-router';

const User = () => import('./views/User.vue');
const UserContainer = () => import('./views/UserContainer.vue');
const AlbumContainer = () => import('./views/AlbumContainer.vue');
const Album = () => import('./views/Album.vue');

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/users',
            name: 'users',
            component: User
        },
        {
            path: '/users/:id',
            name: 'user-page',
            component: UserContainer,
        },
        {
            path: '/albums',
            name: 'albums',
            component: Album,
        },

        {
            path: '/album/:id',
            name: 'album-page',
            component: AlbumContainer,
        },
    ],
});

export default router;
