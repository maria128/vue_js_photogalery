import Vue from 'vue';
import Buefy from 'buefy';
import VuejsDialog from 'vuejs-dialog';

import createFilters from './components/filter/filters';
import router from './router';
import store from './store';
import App from './App.vue';

Vue.config.productionTip = false;
Vue.use(Buefy);
Vue.use(VuejsDialog);
createFilters(Vue);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
